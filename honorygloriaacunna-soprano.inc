\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key d \major

		R1*2  |
		a' 2 a' 4 g'  |
		fis' 2. e' 4  |
%% 5
		d' 2 ~ d' 8 r d' 4  |
		g' 2. g' 4  |
		fis' 2 ~ fis' 8 r d' 4  |
		e' 2. e' 4  |
		d' 1  |
%% 10
		R1  |
		a' 2 a' 4 g'  |
		fis' 2. e' 4  |
		d' 2 ~ d' 8 r d' 4  |
		g' 2. g' 4  |
%% 15
		fis' 2 ~ fis' 8 r d' 4  |
		e' 2. e' 4  |
		d' 1  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		Ho -- nor y glo -- "ria a" ti,
		Se -- ñor Je -- sús,
		Se -- ñor Je -- sús.

		Ho -- nor y glo -- "ria a" ti,
		Se -- ñor Je -- sús,
		Se -- ñor Je -- sús.
	}
>>
