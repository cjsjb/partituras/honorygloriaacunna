\context ChordNames
	\chords {
		\set majorSevenSymbol = \markup { "maj7" }
		\set chordChanges = ##t
		% intro
		d1 d1

		% honor y gloria a ti...
		d1 fis1:7 b1:m g1
		d1 a1 d1

		%
		a1

		% honor y gloria a ti...
		d1 fis1:7 b1:m g1
		d1 a1 d1
	}
