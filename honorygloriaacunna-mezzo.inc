\context Staff = "mezzo" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key d \major

		R1*2  |
		a' 2 a' 4 g'  |
		fis' 2. e' 4  |
%% 5
		d' 2 ~ d' 8 r d' 4  |
		d' 2. d' 4  |
		d' 2 ~ d' 8 r d' 4  |
		cis' 2. cis' 4  |
		a 1  |
%% 10
		R1  |
		a' 2 a' 4 g'  |
		fis' 2. e' 4  |
		d' 2 ~ d' 8 r d' 4  |
		d' 2. d' 4  |
%% 15
		d' 2 ~ d' 8 r d' 4  |
		cis' 2. cis' 4  |
		a 1  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzo" {
		Ho -- nor y glo -- "ria a" ti,
		Se -- ñor Je -- sús,
		Se -- ñor Je -- sús.

		Ho -- nor y glo -- "ria a" ti,
		Se -- ñor Je -- sús,
		Se -- ñor Je -- sús.
	}
>>
